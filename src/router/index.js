import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Contact from "../views/Contact.vue";
import GaleriaFoto from "../views/GaleriaFoto.vue";
import Produse from "../views/Produse.vue";
import Site from "../views/Site.vue";
import SiteFanta from "../views/SiteFanta.vue";
import SiteIndustriale from "../views/SiteIndustriale.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact
  },
  {
    path: "/galeria-foto",
    name: "GaleriaFoto",
    component: GaleriaFoto
  },
  {
    path: "/produse",
    name: "Produse",
    component: Produse
  },
  {
    path: "/site",
    name: "Site",
    component: Site
  },
  {
    path: "/site-fanta",
    name: "SiteFanta",
    component: SiteFanta
  },
  {
    path: "/site-industriale",
    name: "SiteIndustriale",
    component: SiteIndustriale
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
